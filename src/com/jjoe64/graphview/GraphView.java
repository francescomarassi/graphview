package com.jjoe64.graphView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.jjoe64.graphView.compatible.ScaleGestureDetector;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.trackthisfor.android.utils.Utils;

/**
 * GraphView is a Android View for creating zoomable and scrollable graphs.
 * This is the abstract base class for all graphs. Extend this class and implement rawSeries(Canvas, GraphViewData[], float, float, float, double, double, double, double, float)} to display a custom graph.
 * Use {@link LineGraphView} for creating a line chart.
 *
 * @author jjoe64 - jonas gehring - http://www.jjoe64.com
 *
 * Copyright (C) 2011 Jonas Gehring
 * Licensed under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.html
 */
abstract public class GraphView extends LinearLayout {
	protected final Paint paint;
	private String[] verlabels;
	private String[] horlabels;
	private String[] dashboardhorlabels;


	private boolean scrollable;
	public boolean touch_disabled=false;
	public boolean show_labels=true;
	private double viewportStart;
	private double viewportSize;
	private final View viewVerLabels;
	private ScaleGestureDetector scaleDetector;
	private boolean scalable;
	private NumberFormat numberformatter;
	private final List<GraphViewSeries> graphSeries;
	private boolean manualYAxis;
	private double manualMaxYValue;
	private double manualMinYValue;
	protected int number_format;
	Context context;
	protected Double goal=null;
	protected String symbol="";
	public boolean dashboard=false;
	public boolean fill=true;
	public int c=-1;
	public boolean bar=false;
	SimpleDateFormat format_t_point, format_label;
	public GestureDetector gestureDetector;

	static final private class GraphViewConfig {
		static final int BORDER = 10;
		static final int DOWNBORDER = 25;
		static final int realBORDER=BORDER+DOWNBORDER;
		static final int VERTICAL_LABEL_WIDTH = 30;
		static final int HORIZONTAL_LABEL_HEIGHT = 40;
	}
	public void disableTouch(){
		touch_disabled=true;
	}
	public void disableLabels() {
		show_labels=false;

	}
	public void setFormat(int i){
		this.number_format=i;
		format_t_point=Utils.getDateFormat(this.number_format);
		format_label=Utils.getDateFormat(this.number_format, true);
	}
	public void setDashboardChart() {
		dashboard=true;

	}
	private class GraphViewContentView extends View {
		private float lastTouchEventX;
		private float graphwidth;

		/**
		 * @param context
		 */
		public GraphViewContentView(Context context) {
			super(context);
			setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			setupGesture();
		}

		/**
		 * @param canvas
		 */
		@Override
		protected void onDraw(Canvas canvas) {

			paint.setAntiAlias(true);
			paint.setTextSize(px(10));
			// normal
			paint.setStrokeWidth(px(1));
			float border = px(GraphViewConfig.BORDER);
			float upBorder=border;

			float downBorder = px(GraphViewConfig.DOWNBORDER);
			if(show_labels==false){
				downBorder=px(8);
			}
			float horstart = 0;
			float height = getHeight();
			float width = getWidth() - px(20);
			double minY = getMinY();
			double maxY = getMaxY();

			double diffY=generateDiffY(maxY, minY);

			float graphheight = height - (upBorder + downBorder);
			double space=diffY*0.2;




			paint.setARGB(255,196, 203, 212);
			canvas.drawLine(0, border, 0, height-downBorder, paint);
			// vertical lines
			paint.setTextAlign(Align.LEFT);

			if (verlabels == null) {
				verlabels = generateVerlabels();
			}
			int vers = verlabels.length - 1;
			for (int i = 0; i < verlabels.length; i++) {
				float y = ((graphheight / vers) * i) + border;
				canvas.drawLine(horstart, y, getWidth(), y, paint);
			}


			////////// X PART /////////
			double maxX = getMaxX(false);
			double minX = getMinX(false);
			double diffX = maxX - minX;
			graphwidth = width;

			horstart=horstart+px(10);
			if (horlabels == null) {
				horlabels = generateHorlabels(diffX, minX,false);
				dashboardhorlabels = generateHorlabels(diffX, minX, true);
			}

			// horizontal labels + lines
			int hors = horlabels.length-1;
			float ratX=graphwidth/hors;

			if(show_labels){
				for (int i = 0; i < horlabels.length; i++) {
					float x = (ratX * i) + horstart;
					paint.setARGB(255,196, 203, 212);
					paint.setTextAlign(Align.CENTER);
					if (i==horlabels.length-1)
						paint.setTextAlign(Align.RIGHT);
					if (i==0)
						paint.setTextAlign(Align.LEFT);
					if(dashboard){
						paint.setColor(graphSeries.get(0).style.color);
						canvas.drawText(dashboardhorlabels[i], x, px(8), paint);
						paint.setColor(graphSeries.get(1).style.color);
					}else{
						paint.setColor(Color.DKGRAY);
					}
					canvas.drawText(horlabels[i], x, height - px(10), paint);
				}
			}



			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeWidth(px(3));
			int size=graphSeries.size();
			for (int i=0; i<size; i++) {
				paint.setStrokeWidth(graphSeries.get(i).style.thickness);
				paint.setColor(graphSeries.get(i).style.color);
				drawSeries(i, canvas, _values(i), graphwidth, graphheight, border, minX, minY, maxX, maxY, space, diffX, diffY, horstart, graphSeries.get(i).style.color);
			}


		}

		private void onMoveGesture(float f) {
			// view port update
			if (viewportSize != 0) {
				viewportStart -= f*viewportSize/graphwidth;

				// minimal and maximal view limit
				double maxX = getMaxX(true);
				//				if (viewportStart < minX) {
				//					viewportStart = minX;
				//				}else 
				if (viewportStart+viewportSize > maxX) {
					viewportStart = maxX - viewportSize;
					//					Calendar today = Calendar.getInstance();
					//					today.set(Calendar.HOUR_OF_DAY, 0);
					//					viewportStart=(today.getTimeInMillis()-viewportSize);
				}

				// labels have to be regenerated
				verlabels = null;
				horlabels = null;
				//viewVerLabels.invalidate();
			}
			//invalidate();
		}


		public void setupGesture(){
            if(!touch_disabled){
                gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                    public boolean onSingleTapUp(MotionEvent e){
                        //Log.d("Gesture", "onsingleTapUp");
                        if(!touch_disabled){
                            float x = e.getX(e.getActionIndex()); //the location of the touch on the graphview
                            showValueX(x);
                            invalidate();
                        }
                        return true;
                    }
                    public void onLongPress(MotionEvent e) {
                    }
                    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){

                        if(scrollable){
                            showValueX(-1);
                            getParent().requestDisallowInterceptTouchEvent(true);
                            onMoveGesture((-1)*distanceX);
                            invalidate();
                        }
                        return true;
                    }
                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }
                    public boolean onDoubleTap(MotionEvent e){
                        return true;
                    }


                });
            }
		}
		/**
		 * @param event
		 */

		@Override
		public boolean onTouchEvent(MotionEvent event) {
            Log.d("TOUCH EVENT", "onTouchEvent");
            if(!touch_disabled){
                boolean handled=false;
                if (scalable && scaleDetector != null) {
                    scaleDetector.onTouchEvent(event);
                    handled = scaleDetector.isInProgress();
                    if(handled){
                        getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }
                if(!handled){
                    return gestureDetector.onTouchEvent(event);
                }else{
                    return handled;
                }
            }else{
                return false;
            }
		}
	}
	public abstract void showValueX(float x);
	public abstract void resetValueX();

	/**
	 * graph series style: color and thickness
	 */
	static public class GraphViewStyle {
		public int color;
		public int thickness = 3;
		public GraphViewStyle() {
			super();
		}
		public GraphViewStyle(int color, int thickness) {
			super();
			this.color = color;
			this.thickness = thickness;
		}
	}

	/**
	 * one data set for a graph series
	 */
	static public class GraphViewData {
		public final double valueX;
		public final double valueY;
		public float positionX;
		public GraphViewData(double valueX, double valueY) {
			super();
			this.positionX=0;
			this.valueX = valueX;
			this.valueY = valueY;
		}
		public String toString(){
			return String.valueOf(this.valueX) + " --- "+ String.valueOf(this.valueY);
		}
	}

	/**
	 * a graph series
	 */
	static public class GraphViewSeries {
		final String description;
		final GraphViewStyle style;
		final GraphViewData[] values;
		public GraphViewSeries(GraphViewData[] values) {
			description = null;
			style = new GraphViewStyle();
			this.values = values;
		}
		public GraphViewSeries(String description, GraphViewStyle style, GraphViewData[] values) {
			super();
			this.description = description;
			if (style == null) {
				style = new GraphViewStyle();
			}
			this.style = style;
			this.values = values;
		}
	}

	private class VerLabelsView extends View {
		/**
		 * @param context
		 */
		public VerLabelsView(Context context) {
			super(context);
			//setLayoutParams(new LayoutParams(GraphViewConfig.VERTICAL_LABEL_WIDTH, LayoutParams.MATCH_PARENT));
		}

		/**
		 * @param canvas
		 */
		@SuppressLint("DrawAllocation")
		@Override
		protected void onDraw(Canvas canvas) {

			// normal
			paint.setStrokeWidth(px(1));

			float real_border = px(GraphViewConfig.realBORDER);
			if(show_labels==false){
				real_border=real_border-px(17);
			}
			float height = getHeight();
			float graphheight = height - (real_border);

			if (verlabels == null) {
				verlabels = generateVerlabels();
			}
			int length=0;
			for(int i=0; i<verlabels.length;i++){
				if(verlabels[i].length()>length){
					length=verlabels[i].length();
				}
			}
			// vertical labels
			paint.setTextAlign(Align.RIGHT);
			paint.setColor(Color.DKGRAY);
			paint.setAntiAlias(true);
			paint.setTextSize(px(11));
			int width=GraphViewConfig.VERTICAL_LABEL_WIDTH;

			if(length>1){
				width=(int) (width+px(5));
			}else{
				//width=(int) (width-px(15));
			}
			if(length>3){
				paint.setTextSize(px(9));
				width=(int) (width+px(10));
			}
			if(length>5){
				width=(int) (width+px(10));
			}
			this.setLayoutParams(new LayoutParams(width, LayoutParams.MATCH_PARENT));

			// normal
			int vers = verlabels.length - 1;
			for (int i = 0; i < verlabels.length; i++) {
				float y = ((graphheight / vers) * i) + px(GraphViewConfig.BORDER);

				canvas.drawText(verlabels[i],width-px(2), y+5, paint);

			}

		}
	}


	/**
	 *
	 * @param context
	 */
	public GraphView(Context context) {
		super(context);
		this.context=context;
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		paint = new Paint();

		paint.setAntiAlias(true);
		graphSeries = new ArrayList<GraphViewSeries>();

		viewVerLabels = new VerLabelsView(context);
		addView(viewVerLabels);
		addView(new GraphViewContentView(context), new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1));
	}
	public void setGoal(Double g){
		this.goal=g;
	}
	public void setSymbol(String s){
		this.symbol=s;
	}
	protected GraphViewData[] _values(int idxSeries) {
		GraphViewData[] values = graphSeries.get(idxSeries).values;
		return values;
	}

	public void addSeries(GraphViewSeries series) {
		graphSeries.add(series);
	}

	public void removeSeries(int index)
	{
		if (index < 0 || index >= graphSeries.size())
		{
			throw new IndexOutOfBoundsException("No series at index " + index);
		}

		graphSeries.remove(index);
	}

	public void removeSeries(GraphViewSeries series)
	{
		graphSeries.remove(series);
	}

	abstract public void drawSeries(int position, Canvas canvas, GraphViewData[] values, float graphwidth, float graphheight, float border, double minX, double minY, double maxX, double maxY, double space, double diffX, double diffY, float horstart, int color);

	/**
	 * formats the label
	 * can be overwritten
	 * @param value x and y values
	 * @param isValueX if false, value y wants to be formatted
	 * @return value to display
	 */
	protected String formatLabel(double value, boolean isValueX) {
		if (numberformatter == null) {
			numberformatter = NumberFormat.getNumberInstance();
			double highestvalue = getMaxY();
			double lowestvalue = getMinY();
			//			if (highestvalue - lowestvalue < 1) {
			//				numberformatter.setMaximumFractionDigits(2);
			//			} else 
			if (highestvalue - lowestvalue < 5) {
				numberformatter.setMaximumFractionDigits(1);
			}else{
				numberformatter.setMaximumFractionDigits(0);
			}
		}
		return numberformatter.format(value);
	}
	protected boolean is_precise_measure(double max, double min){
		if(max==min)
			return false;
		if((max-min)<min/2){
			return true;
		}else{
			return false;
		}
	}
	protected double getPreciseMinY(double max, double min){
		return min-((max-min)/2);
	}
	protected double getPreciseMaxY(double max, double min){
		return max+(max-min)/2;
	}

	private String[] generateHorlabels(double diffX2, double min2, boolean dashboard) {
		int numLabels = 6; //(int) (graphwidth / GraphViewConfig.VERTICAL_LABEL_WIDTH);
		String[] labels = new String[numLabels + 1];
		double min=getMinX(false);
		double max=getMaxX(false);
		double diffX=max-min;
		long remove=0;
		if(dashboard)
			remove=Utils.dashboardRange(c);
		for (int i = 0; i <= numLabels; i++) {
			double resolvedX = min + (((diffX) * i)/numLabels);
			//labels[i] = this.formatLabel(existentX, true);
			labels[i]=format_label.format(new Date((long) resolvedX-remove));
		}
		return labels;
	}

	synchronized private String[] generateVerlabels() {
		int numLabels = 5;
		String[] labels = new String[numLabels+1];
		double min = getMinY();
		double real_min=min;
		double max = getMaxY();

		if(min>=0){
			if(!bar){
				if(is_precise_measure(max, min)){
					//max=max*1.2;
					min=min-(max-min);
					max=max+(max-real_min)*2;
				}else{
					min=0;
					real_min=0;
					max=max*1.3;
				}
			}
			for (int i=0; i<=numLabels; i++) {
				labels[numLabels-i] = formatLabel(min + ((max-real_min)*i/numLabels), false);
			}

		}else{
			for (int i=0; i<=numLabels; i++) {
				labels[numLabels-i] = formatLabel(min + ((max-min)*i/numLabels), false);
			}
		}

		return labels;
	}


	/**
	 * returns the maximal X value of the current viewport (if viewport is set)
	 * otherwise maximal X value of all data.
	 * @param ignoreViewport
	 *
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMaxX(boolean ignoreViewport) {
		// if viewport is set, use this
		if(!ignoreViewport && viewportSize != 0){
			return viewportStart+viewportSize;
		}else{
			double highest = 0;
			if (graphSeries.size() > 0)
			{
				GraphViewData[] values;
				for (int i=0; i<graphSeries.size(); i++) {
					values = graphSeries.get(i).values;
					if(values.length>0){
						highest = Math.max(highest, values[values.length-1].valueX);
					}
				}
			}
			return highest;
		}
	}


	/**
	 * returns the minimal X value of the current viewport (if viewport is set)
	 * otherwise minimal X value of all data.
	 * @param ignoreViewport
	 *
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMinX(boolean ignoreViewport) {
		// if viewport is set, use this
		if (!ignoreViewport && viewportSize != 0) {
			return viewportStart;
		} else {
			// otherwise use the min x value
			// values must be sorted by x, so the first value has the smallest X value
			double lowest = Double.MAX_VALUE;
			if (graphSeries.size() > 0)
			{
				GraphViewData[] values;
				for (int i=0; i<graphSeries.size(); i++) {
					values = graphSeries.get(i).values;
					if(values.length>0){
						lowest = Math.min(lowest, values[0].valueX);
					}
				}
			}
			return lowest;
		}
	}

	/**
	 * returns the maximal Y value of all data.
	 *
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMaxY() {
		double largest;
		if (manualYAxis & (manualMaxYValue >= manualMinYValue)) {
			largest = manualMaxYValue;
		} else {

			largest = Integer.MIN_VALUE;
			for (int i=0; i<graphSeries.size(); i++) {
				GraphViewData[] values = _values(i);
				for (int ii=0; ii<values.length; ii++)
					if (values[ii].valueY > largest)
						largest = values[ii].valueY;
			}
			manualMaxYValue=largest;
		}
		if((goal!=null) && largest<goal){
			return goal;
		}else{
			return largest;
		}
	}
	/**
	 * returns the minimal Y value of all data.
	 *
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMinY() {
		double smallest;
		if (manualYAxis & (manualMaxYValue >= manualMinYValue)) {
			smallest = manualMinYValue;
		} else {
			smallest = Integer.MAX_VALUE;
			for (int i=0; i<graphSeries.size(); i++) {
				GraphViewData[] values = _values(i);
				for (int ii=0; ii<values.length; ii++)
					if (values[ii].valueY < smallest)
						smallest = values[ii].valueY;
			}
			manualMinYValue=smallest;
		}
		if(goal!=null && smallest>goal){
			return goal;
		}else{
			return smallest;
		}
	}
	public boolean isScrollable() {
		return scrollable;
	}





	/**
	 * you have to set the bounds {@link #setManualYAxisBounds(double, double)}. That automatically enables manualYAxis-flag.
	 * if you want to disable the menual y axis, call this method with false.
	 * @param manualYAxis
	 */
	public void setManualYAxis(boolean manualYAxis) {
		this.manualYAxis = manualYAxis;
		this.setManualYAxisBounds();
	}


	public void setManualYAxisBounds() {
		manualMaxYValue = Integer.MIN_VALUE;
		manualMinYValue = 1;
		manualYAxis = true;
	}

	public void setManualYAxisBounds(double max, double min) {
		manualMaxYValue = max;
		manualMinYValue = min;
		manualYAxis = true;
	}

	/**
	 * this forces scrollable = true
	 * @param scalable
	 */
	synchronized public void setScalable(boolean scalable) {
		this.scalable = scalable;
		if (scalable == true && scaleDetector == null) {
			scrollable = true; // automatically forces this
			scaleDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.SimpleOnScaleGestureListener() {
				@Override
				public boolean onScale(ScaleGestureDetector detector) {
					double center = viewportStart + viewportSize / 2;
					viewportSize /= detector.getScaleFactor();
					viewportStart = center - viewportSize / 2;

					// viewportStart must not be < minX
					double minX = getMinX(true);
					if (viewportStart < minX) {
						viewportStart = minX;
					}

					// viewportStart + viewportSize must not be > maxX
					double maxX = getMaxX(true);
					double overlap = viewportStart + viewportSize - maxX;
					if (overlap > 0) {
						// scroll left
						if (viewportStart-overlap > minX) {
							viewportStart -= overlap;
						} else {
							// maximal scale
							viewportStart = minX;
							viewportSize = maxX - viewportStart;
						}
					}

					verlabels = null;
					numberformatter = null;
					invalidate();
					//viewVerLabels.invalidate();
					return true;
				}
			});
		}
	}

	/**
	 * the user can scroll (horizontal) the graph. This is only useful if you use a viewport {@link #setViewPort(double, double)} which doesn't displays all data.
	 * @param scrollable
	 */
	public void setScrollable(boolean scrollable) {
		this.scrollable = scrollable;
	}



	/**
	 * set's static vertical labels (from top to bottom)
	 * @param verlabels if null, labels were generated automatically
	 */
	public void setVerticalLabels(String[] verlabels) {
		this.verlabels = verlabels;
	}

	/**
	 * set's the viewport for the graph.
	 * @param start x-value
	 * @param size
	 */
	public void setViewPort(double start, double size) {
		viewportStart = start;
		viewportSize = size;
	}
	public float px(float dp){
		DisplayMetrics metrics = this.context.getResources().getDisplayMetrics();
		return (metrics.density * dp + 0.5f);
	}
	public void setContext(int c2) {
		this.c=c2;
	}
	public double generateDiffY(double maxY, double minY){
		double diffY=0;
		if(bar){
			return maxY-minY;
		}else{
		if(maxY<0){
			//minY e maxY entrambi negativi
			diffY=Math.abs(minY-maxY);

		}else if(minY<0){
			//minY negativo, maxY positivo
			diffY=(maxY)+Math.abs(minY);

		}else{
			//minY and maxY entrambi positivi
			//diffY=(maxY)-(minY);
			if(is_precise_measure(maxY, minY)){
				//diffY=maxY*1.2-minY*1.1;
				diffY=maxY-minY+2*(maxY-minY);
			}else{
				diffY=maxY*1.3;
			}
		}
		return diffY;
	
		}
	}
	public void disableFill() {
		fill=false;
	}

}
