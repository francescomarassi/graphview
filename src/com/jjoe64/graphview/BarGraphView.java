package com.jjoe64.graphView;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import me.trackthisfor.android.utils.Utils;


/**
 * Draws a Bar Chart
 * @author Muhammad Shahab Hameed
 */
public class BarGraphView extends GraphView {

	Paint fill_style = new Paint() {{
		setStyle(Paint.Style.FILL);
		setAntiAlias(true);
		setStrokeWidth(px(0));


	}};

	public BarGraphView(Context context) {
		super(context);
		bar=true;
	}

	@Override
	public void showValueX(float x) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resetValueX() {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawSeries(int position, Canvas canvas, GraphViewData[] values,
			float graphwidth, float graphheight, float border, double minX,
			double minY, double maxX, double maxY, double space, double diffX,
			double diffY, float horstart, int color) {
		paint.setColor(color);
		float colwidth = (graphwidth - (2 * border)) / (Utils.getContextDays(this.c));
        Log.d("drawSeries", "BargraphSeries");
		for (int i = 0; i < values.length; i++) {
			float valY = (float) (values[i].valueY - minY);
			float ratY = (float) (valY / diffY);
			float y = graphheight * ratY;
			double valX = values[i].valueX - minX;
			double ratX = valX / diffX;
			float x=(float) (graphwidth*ratX);
			canvas.drawRect(
					x ,
					(border - y) + graphheight,
					x + colwidth - 1,
					graphheight + border ,
					paint
					);
		}




	}
}