package com.jjoe64.graphView;


import java.text.NumberFormat;
import java.util.Date;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Align;

/**
 * Line Graph View. This draws a line chart.
 * @author jjoe64 - jonas gehring - http://www.jjoe64.com
 *
 * Copyright (C) 2011 Jonas Gehring
 * Licensed under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.html
 */
public class LineGraphView extends GraphView {
	private int CIRCLERADIUS=7;
	TouchedPoint t_point;
	NumberFormat numberFormatter = NumberFormat.getNumberInstance();

	Paint fill_style = new Paint() {{
		setStyle(Paint.Style.FILL);
		setAntiAlias(true);
		setStrokeWidth(px(0));
	}};
	Paint goal_line = new Paint() {{
		setStyle(Paint.Style.STROKE);
		setAntiAlias(true);
		setStrokeWidth(px(1));
	}};
	Paint touch_line = new Paint() {{
		setAntiAlias(true);
		setColor(Color.DKGRAY);
		setStrokeWidth(0.5f);
	}};
	Paint touch_line_text = new Paint() {{
		setStyle(Paint.Style.STROKE);
		setTextSize(px(11));
		setAntiAlias(true);
		setColor(Color.DKGRAY);
		setTextAlign(Align.CENTER);
	}};

	Paint lineBorder = new Paint() {{
		setStyle(Paint.Style.STROKE);
		setAntiAlias(true);
		setStrokeWidth(px(3));
		setStrokeCap(Cap.ROUND);
	}};
	Paint white= new Paint() {{
		setARGB(255, 255,255,255);
		setAntiAlias(true);
	}};
	public LineGraphView(Context context) {
		super(context);
		t_point=new TouchedPoint();
		numberFormatter.setMaximumFractionDigits(2);

	}
	public class Point{
		public float x;
		public float y;
		public Point(float x, float y){
			this.x=x;
			this.y=y;
		}
		public Point(){

		}
		public void set(float x, float y){
			this.x=x;
			this.y=y;
		}
	}
	@Override

	public void drawSeries(int position, Canvas canvas, GraphViewData[] values, float graphwidth, float graphheight, float border, double minX, double minY, double maxX, double maxY ,double space, double diffX, double diffY, float horstart, int line_color) {
		lineBorder.setColor(line_color);
		fill_style.setColor(line_color);
		goal_line.setColor(line_color);
		fill_style.setAlpha(75);
		double lastEndY = 0;
		double lastEndX = 0;
		if(values.length>30){
			CIRCLERADIUS=5;
		}

		double zero=0;
		float originY=0;
		double base=0;
		if(minY>0 && is_precise_measure(maxY,minY)){
			zero=0;
			originY=(float)(border+graphheight-zero);
			base=minY-(maxY-minY);
		}else{
			zero=minY<0 ? Math.abs(minY) : 0;
			zero=(zero*graphheight)/diffY;
			originY=(float)(border+ graphheight-zero);
			base=(minY<0 ? minY : 0);
		}
		int size=values.length;
		for (int i = 0; i < size; i++) {

			Path line_path=new Path();
			Path fill_path=new Path();

			double valY = values[i].valueY-base;
			double ratY = (valY) / (diffY);

			double y = (graphheight * ratY);


			double valX = values[i].valueX - minX;
			//Log.d("valX", ""+valX);
			double ratX = valX / diffX;

			//Log.d("horstart", ""+horstart);
			float endX=0;
			float endY=0;
			double x = graphwidth * ratX;
			if (i > 0) {
				float startX = (float) lastEndX + (horstart);
				float startY = (float) (border - lastEndY) + graphheight;

				fill_path.moveTo((float) startX, originY);
				fill_path.lineTo((float)startX, (float)(startY));
				endX = (float) x + (horstart);
				endY = (float) (border - y) + graphheight;

				doBezier(canvas, new Point(startX, startY), new Point(endX, endY), fill_path, true);
				doBezier(canvas, new Point(startX, startY), new Point(endX, endY), line_path, false);
			}else{
				fill_path.moveTo((float)x+(horstart),originY);
				fill_path.lineTo((float)x+(horstart),(float)(border+ graphheight-y));
				line_path.moveTo((float)x+(horstart),(float)(border+ graphheight-y));
			}

			canvas.drawPath(line_path, lineBorder);

			endX = (float) x + (horstart);
			lastEndY = y;
			lastEndX = x;
			fill_path.lineTo((float)endX, originY);
			fill_path.close();
			if(fill){
				canvas.drawPath(fill_path, fill_style);
			}
		}



		// draw data
		lastEndY = 0;
		lastEndX = 0;
		if(minY<0){	
			//disegno riga per y=0
			paint.setARGB(255, 195, 195, 195);
			canvas.drawLine(horstart-px(20), originY, horstart+graphwidth-px(20),  (float)(border+graphheight-zero), paint);
			paint.setColor(line_color);
		}
		if(goal!=null){
			double goal_y= graphheight*(((double)goal-base)/diffY);
			goal_y=(border - goal_y) + graphheight;
			canvas.drawLine(0, (float)(goal_y), graphwidth+horstart,(float)(goal_y), goal_line);

		}
		if(t_point.active & !this.touch_disabled){
			if(t_point.i==0){
				touch_line_text.setTextAlign(Align.LEFT);
			}
			if(t_point.i==size-1){
				touch_line_text.setTextAlign(Align.RIGHT);
			}
			canvas.drawLine((float)t_point.position, (float)px(10), (float)t_point.position,graphheight+px(10), touch_line);	
			canvas.drawText(""+numberFormatter.format(t_point.valueY)+" "+ symbol + " - " +format_t_point.format(new Date((long)t_point.valueX)), (float)t_point.position, (float)px(8), touch_line_text);
			touch_line_text.setTextAlign(Align.CENTER);
		}
		for (int i = 0; i < size; i++) {

			double valY = values[i].valueY - base;
			diffY= (diffY==0 ? 1 : diffY);
			diffX=(diffX==0 ? 1 : diffX);
			double ratY = valY / diffY;
			double y = graphheight * ratY;

			double valX = values[i].valueX - minX;
			double ratX = valX / diffX;
			double x = graphwidth * ratX;
			float endX = (float) x + (horstart + 1);
			float endY = (float) (border - y) + graphheight;

			if(size==1 && !dashboard){
				paint.setStrokeWidth(0.5f);
				paint.setColor(Color.DKGRAY);
				canvas.drawLine(endX, endY+px(8), endX,graphheight+px(10), paint);
			}
			paint.setStrokeWidth(2);
			paint.setColor(line_color);

			//aggiungo la posizione attuale del punto, in modo che posso riprenderla quando cerco il valueX;
			values[i].positionX=endX;
			//draw cirle
			canvas.drawCircle(endX, endY, px(CIRCLERADIUS), paint);
			canvas.drawCircle(endX, endY, px(CIRCLERADIUS-3), white);
		}


	}
	public void fill_t_point(float x){
		GraphViewData[] values=_values(0);
		int i=-1;
		int size=values.length;
		do{
			i=i+1;
		}while(values[i].positionX<x & i<size-1);
		//ora so che il punto toccato x è in mezzo ai due valori che potrebbero rappresentarlo;
		//confronto i due valori così capisco quale dei due è più vicino
		if(i<1){
			t_point.fill_data(values[i], i);
		}else{
			GraphViewData valoreMaggiore =values[i];
			GraphViewData valoreMinore =values[i-1];
			if(valoreMaggiore.positionX-x > x-valoreMinore.positionX){
				//il valoreMinore è il più vicino;

				t_point.fill_data(valoreMinore, i-1);
			}else{
				//il valoreMaggiore è il più vicino;
				t_point.fill_data(valoreMaggiore, i);
			}
		}

	}
	class TouchedPoint{
		public float position;
		public double valueX;
		public double valueY;
		public int i;
		public boolean active=false;
		public void fill_data(GraphViewData data, int i){
			this.valueX=data.valueX;
			this.valueY=data.valueY;
			this.position=data.positionX;
			this.i=i;
		}

	}
	public void showValueX(float x){
		t_point.active=true;
		fill_t_point(x);
	}
	public void resetValueX(){
		t_point.active=false;
	}

	public void doBezier(Canvas canvas, Point start, Point end, Path p, Boolean shape){

		if(!shape){
			p.moveTo(start.x, start.y);
		}
		Point mid = new Point();

		mid.set((start.x + end.x) / 2, (start.y + end.y) / 2);

		// Draw line connecting the two points:

		p.quadTo((start.x + mid.x) / 2, start.y, mid.x, mid.y);
		p.quadTo((mid.x + end.x) / 2, end.y, end.x, end.y);


	}
}
